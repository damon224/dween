import discord
import os
from random import choice

from dotenv import load_dotenv

load_dotenv()
SECRET_DISCORD_KEY = os.getenv('SECRET_DISCORD_KEY')

WORKING_CHANNELS = ['emotional_support']
GAME_CHOICES = ['rock', 'paper', 'scissors']

SUPPORT_MESSAGES = [
    "This situation must feel awful, but Remco is here if you need a friend to listen to as you bounce back",
    "You have been there for me during *rock* hard times, and now Timothy is here for you",
    "I am proud of everything you have done and how you are handling this situation ",
    "I know you are in pain right now, but Ivo is here if you need someone that will listen to you",
    "Things may not have turned out the way you wanted them to, but I know you can handle it",
    "Your support bot cares about you and loves you, and I am here whenever you are ready",
    "I believe in you, and I hope you believe in yourself just as much as I do",
    "I know you are strong enough to do this alone, but you do not have to. Frank is here",
    "I know things are hard right now, but you are strong enough to get through this",
    "You have the power to overcome anything, and I know you will get there",
    "I know you have a lot on your plate, so what can Tibor do to help?",
    "I love you <3",
    """Lorem ipsum dolor sit amet consectetur adipisicing elit. Maxime mollitia,
molestiae quas vel sint commodi repudiandae consequuntur voluptatum laborum
numquam blanditiis harum quisquam eius sed odit fugiat iusto fuga praesentium
optio, eaque rerum! Provident similique accusantium nemo autem. Veritatis
obcaecati tenetur iure eius earum ut molestias architecto voluptate aliquam
nihil, eveniet aliquid culpa officia aut! Impedit sit sunt quaerat, odit,
tenetur error, harum nesciunt ipsum debitis quas aliquid. Reprehenderit,
quia. Quo neque error repudiandae fuga? Ipsa laudantium molestias eos 
sapiente officiis modi at sunt excepturi expedita sint? Sed quibusdam
recusandae alias error harum maxime adipisci amet laborum. Perspiciatis 
minima nesciunt dolorem! Officiis iure rerum voluptates a cumque velit 
quibusdam sed amet tempora. Sit laborum ab, eius fugit doloribus tenetur 
fugiat, temporibus enim commodi iusto libero magni deleniti quod quam 
consequuntur! Commodi minima excepturi repudiandae velit hic maxime
doloremque. Quaerat provident commodi consectetur veniam similique ad 
earum omnis ipsum saepe, voluptas, hic voluptates pariatur est explicabo 
fugiat, dolorum eligendi quam cupiditate excepturi mollitia maiores labore 
suscipit quas? Nulla, placeat. Voluptatem quaerat non architecto ab laudantium
modi minima sunt esse temporibus sint culpa, recusandae aliquam numquam 
totam ratione voluptas quod exercitationem fuga. Possimus quis earum veniam 
quasi aliquam eligendi, placeat qui corporis!"""
]

STONER_QUOTES = [
    'If we all had a bong, we’d all get along.',
    'When you smoke the herb, it reveals you to yourself.',
    '“I don\'t think [pot] is more dangerous than alcohol.” — Barack Obama',
    'Breathe in, hold it, puff out, and grin.',
    'I never joined high school, I joined school high.',
    'Don’t worry. Don’t cry. Smoke weed and get high.'
]


intents = discord.Intents.default()
intents.message_content = True

client = discord.Client(intents=intents)


@client.event
async def on_ready():
    print(f'{client.user} has connected to Discord!')

@client.event
async def on_message(message):
    if message.author == client.user or str(message.channel) not in WORKING_CHANNELS or message.content in GAME_CHOICES:
        return

    if message.content == "bing":
        await message.channel.send("bong")
        return
        
    print(f"{message.author}: {message.content}")
    print(client.user)
    if message.content == "!dween play":
        await message.channel.send("Make your choice! ('rock', 'paper', 'scissors')")

        user_choice = (await client.wait_for('message')).content

        if 'rock' in user_choice.lower():
            await message.channel.send('I chose rock aswell! It\'s a tie!')
        elif 'paper' in user_choice.lower():
            await message.channel.send('I chose rock! You win!')
        elif 'scissor' in user_choice.lower():
            await message.channel.send('I chose rock! I win!')

        return

    if str(message.author).lower() == "rawrbin#2899":
        await message.channel.send("You'll find your answer at Topweg 31")
        return

    if str(message.author).lower() == "nyroque#8332":
        await message.channel.send(choice(STONER_QUOTES))
        return

    if str(message.author).lower() == "timothy#2735":
        await message.channel.send("I love you dad <3")
        return

    await message.channel.send(choice(SUPPORT_MESSAGES))

client.run(SECRET_DISCORD_KEY)